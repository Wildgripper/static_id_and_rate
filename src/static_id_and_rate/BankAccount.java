package static_id_and_rate;

public class BankAccount {

    private int id;
    private double balance;
    private double interest = 0.02;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setInterestRate(double newInterest) {
        this.interest = newInterest;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getInterest(int years) {
        double sum = balance;

        for (int i = 1; i < years; i++) {
            sum = sum + (sum * interest);
        }
        return sum;
    }


}
