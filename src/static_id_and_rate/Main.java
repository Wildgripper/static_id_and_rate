package static_id_and_rate;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Bank bank = new Bank();
        BankAccount acc = new BankAccount();
        String command = scanner.nextLine().toLowerCase();

        while (!command.equals("end")) {
            String[] cmdArgs = command.split("\\s+");
            String cmdType = cmdArgs[0];
            int id = 0;
            double amount = 0;

            if (cmdArgs.length == 2) {
                id = Integer.valueOf(cmdArgs[1]);
            } else if (cmdArgs.length == 3) {
                id = Integer.valueOf(cmdArgs[1]);
                amount = Double.valueOf(cmdArgs[2]);
            }

            switch (cmdType) {
                case "create":
                    bank.create();
                    break;
                case "deposit":
                    if (bank.getBankAccount(id) == null) {
                        break;
                    }
                    bank.getBankAccount(id).deposit(amount);
                    System.out.printf("Deposited %.0f to ID%d%n", amount, id);
                    break;
                case "setinterest":
                    if (bank.getBankAccount(id) == null) {
                        break;
                    }
                    bank.getBankAccount(id).setInterest(amount);
                    System.out.printf("Interest for ID%d is set to %.2f%n", id, amount);
                    break;
                case "getinterest":
                    if (bank.getBankAccount(id) == null) {
                        break;
                    }
                    int years = (int)amount;
                    double interest = bank.getBankAccount(id).getInterest(years);
                    System.out.printf("Interest for %d years is %.2f%n", years, interest);
                    break;
            }

            command = scanner.nextLine();
        }

    }


}
