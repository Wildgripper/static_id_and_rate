package static_id_and_rate;

import java.util.HashMap;

public class Bank {
    private HashMap<Integer, BankAccount> accounts = new HashMap<>();
    private int lastAccountId = 1;

    public void create() {
        BankAccount account = new BankAccount();
        account.setId(lastAccountId);
        accounts.put(lastAccountId ++, account);
        System.out.printf("Account ID%d created%n", account.getId());
    }

    public BankAccount getBankAccount(int id){
        if (accounts.containsKey(id)) {
          return   accounts.get(id);
        }
            System.out.println("Account does not exist!%n");
        return null;
    }


}
