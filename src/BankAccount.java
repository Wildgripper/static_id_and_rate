import java.util.HashMap;

public class BankAccount {

    private int id;
    private double balance;
    private HashMap<Integer, BankAccount> accounts = new HashMap<>();

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return this.balance;
    }

    public void deposit(double amount) {
        if (amount <= 0) {
            System.out.println("Please enter correct amount!");
            return;
        }
        balance += amount;
    }

    public void withdraw(double amount) {
        if (amount <= 0) {
            System.out.println("Please enter correct amount!");
            return;
        }
        this.balance -= amount;
    }

    @Override
    public String toString() {
        return "ID" + this.id;
    }

    public void create(String[] cmdArgs) {
        BankAccount account = new BankAccount();
        int id = Integer.valueOf(cmdArgs[1]);
        if (accounts.containsKey(id)) {
            System.out.println("Account already exists!");
        } else {
            account.setId(id);
            accounts.put(id, account);
        }
    }

    public void deposit(String[] cmdArgs) {
        int id = Integer.valueOf(cmdArgs[1]);
        double amount = Double.valueOf(cmdArgs[2]);

        if (accounts.containsKey(id)) {
            accounts.get(id).balance += amount;
        } else {
            System.out.println("Account does not exist!");
        }

    }

    public void print(String[] cmdArgs) {
        int id = Integer.valueOf(cmdArgs[1]);

        if (accounts.containsKey(id)) {
            System.out.printf("Account ID%d, balance %.2f", id, accounts.get(id).balance);
        } else {
            System.out.println("Account does not exist!");
        }

    }

    public void withdraw(String[] cmdArgs) {
        int id = Integer.valueOf(cmdArgs[1]);
        double amount = Double.valueOf(cmdArgs[2]);

        if (accounts.containsKey(id)) {
            if (accounts.get(id).balance >= amount) {
                accounts.get(id).balance -= amount;
            } else {
                System.out.println("Insufficient balance");
            }
        } else {
            System.out.println("Account does not exist!");
        }

    }

}
