import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        BankAccount acc = new BankAccount();
        String command = scanner.nextLine().toLowerCase();

        while (!command.equals("end")) {
            String[] cmdArgs = command.split("\\s+");
//            System.out.println(Arrays.toString(cmdArgs));
            String cmdType = cmdArgs[0];

            switch (cmdType) {
                case "create":
                    acc.create(cmdArgs);
                    break;
                case "deposit":
                    acc.deposit(cmdArgs);
                    break;
                case "print":
                    acc.print(cmdArgs);
                    break;
                case "withdraw":
                    acc.withdraw(cmdArgs);
                    break;
            }


            command = scanner.nextLine();
        }


//            acc.setId(1);
//            acc.deposit(15);
//            acc.withdraw(5);
//
//            acc.create(1);
//            acc.create(2);
//            acc.create(3);
//
//            acc.deposit(2, 10);
//            acc.print(2);

//        System.out.println(acc.accounts.get(2));
//        System.out.println(acc2.accounts.get(2));
//        System.out.printf("Account %s, balance %.2f%n", acc, acc.getBalance());
    }
}
